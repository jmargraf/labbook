# Importantly Wrong

> Since all models are wrong the scientist must be alert to what is importantly wrong. It is inappropriate to be concerned about mice when there are tigers abroad.
>
> -- [George E. P. Box](https://doi.org/10.1080%2F01621459.1976.10480949)

This blog is basically a collection of Jupyter Notebooks dealing with different aspects of materials modelling, often related to machine learning. It's intended for teaching myself and others.

There currently is no comment function, since the blog is implemented as a Jupyter Book. Feel free to direct any feedback [this way](mailto:margraf@fhi.mpg.de).  

